// content.js


function applyBionicReading(element) {
  // Helper function to determine the number of characters to make bold
  function getBoldCount(word) {
    // Adjust the factor as needed based on your preferences
    return Math.floor(word.length / 2);
  }

  // Helper function to make the specified number of characters bold
  function makeTextBold(textNode) {
    const words = textNode.textContent.split(/\s+/);
    const modifiedText = words
      .map(word => {
        if (word.length > 0) {
          const boldCount = getBoldCount(word)
          const boldPart = word.substring(0, boldCount);
          const remainingPart = word.substring(boldCount);
          return `<b>${boldPart}</b>${remainingPart}`;
        } else {
          return '';
        }
      })
      .join(' ');

    // Update the text content with the modified text
    textNode.replaceWith(document.createRange().createContextualFragment(modifiedText));
  }

  // Collect text nodes from the top-level element
  const textNodes = [];
  function collectTextNodes(node) {
    if (node.nodeType === Node.TEXT_NODE) {
      textNodes.push(node);
    } else if (node.nodeType === Node.ELEMENT_NODE) {
      for (const childNode of node.childNodes) {
        collectTextNodes(childNode);
      }
    }
  }
  collectTextNodes(element);

  // Apply bionic reading to each collected text node
  textNodes.forEach(textNode => {
    const words = textNode.textContent.split(/\s+/);
    makeTextBold(textNode);
  });
}







// content.js


let bionicReadingEnabled = false;

// Listen for messages from the popup
chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {
  if (request.action === 'enableBionicReading') {
    bionicReadingEnabled = true;
    const paragraphs = document.querySelectorAll('p, span');
    paragraphs.forEach((paragraph) => {
      applyBionicReading(paragraph);
    });
    chrome.tabs.reload();
  } else if (request.action === 'disableBionicReading') {
    bionicReadingEnabled = false;
    // You may want to reset or undo any modifications here
    chrome.tabs.reload();
  }
});


