// popup.js

document.addEventListener('DOMContentLoaded', function() {
  const bionicReadingOnRadio = document.getElementById('bionicReadingOn');
  const bionicReadingOffRadio = document.getElementById('bionicReadingOff');

  // Listen for changes in the radio buttons
  bionicReadingOnRadio.addEventListener('change', function() {
    // Send a message to content.js indicating that bionic reading should be enabled
    chrome.tabs.query({ active: true, currentWindow: true }, function(tabs) {
      chrome.tabs.sendMessage(tabs[0].id, { action: 'enableBionicReading' });
    });
  });

  bionicReadingOffRadio.addEventListener('change', function() {
    // No need to send a message when "Bionic Reading Off" is selected
  });
});

