# Bionic Hero Reader

Chrome extension to apply Bionic Reading to any webpage. It basically helps you read faster. And it's a must for ADHD.

***

## Usage
- Click On to apply Bionic Reading to the current webpage
- Click Off and refresh your page to disable Bionic Reading

***

## Authors and acknowledgment

Jean-Sébastien Patenaude
